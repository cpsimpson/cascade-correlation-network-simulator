from typing import List, Dict

from spiral_data import partitioned_spirals

from proben1_loader import load_problem

from pycascor.activation_functions import Sigmoid, AsymmetricSigmoid, ActivationFunction
from pycascor.trainer import Trainer, PruneTrainer, OptimalBrainDamageTrainer
from pycascor.network import CCNetwork, PrunableCCNetwork
import argparse
import csv

from time import time
from pathlib import Path
from functools import reduce
from itertools import chain

import numpy as np


from pickle import dump
from enum import Enum

from matplotlib import pyplot as plt

import logging

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)


class NetworkType(Enum):
    StandardNetwork = 1,
    PrunableNetwork = 2,
    OptimalBrainDamageNetwork = 3,

#
# def unique_filename(prefix, extension, output_dir="./result"):
#     Path(output_dir).mkdir(parents=True, exist_ok=True)
#     return f"{output_dir}/{unique_id(prefix)}.{extension}"


def unique_id():
    return f"{time()}"


def proportion_correct(expected: list, results: list, asymmetric: bool) -> float:
    correct = 0
    for trial_index in range(len(expected)):
        if asymmetric:
            correct += reduce(lambda x, y: x + y,
                              map(lambda p, q: 1 if round(p, 0) == round(q, 0) else 0, expected[trial_index],
                                  results[trial_index]))
        else:
            correct += reduce(lambda x, y: x + y,
                              map(lambda p, q: 1 if np.sign(p) == np.sign(q) else 0, expected[trial_index],
                                  results[trial_index]))
    return correct / (len(expected) * len(expected[0]))


def train(training_inputs: list, training_outputs: list, input_nodes: int = 9,
          output_nodes: int = 2, save_network: bool = False,
          candidate_pool_size: int = 8,
          network_type: NetworkType = NetworkType.StandardNetwork,
          activation_function: ActivationFunction = Sigmoid,
          output_function: ActivationFunction = Sigmoid, output_dir: str = ".") -> tuple[CCNetwork, str, int]:
    if network_type == NetworkType.StandardNetwork:
        trainer = Trainer([(candidate_pool_size, activation_function)])
        net = CCNetwork(input_nodes, output_nodes, output_function)
    elif network_type == NetworkType.PrunableNetwork:
        trainer = PruneTrainer([(candidate_pool_size, activation_function)])
        net = PrunableCCNetwork(input_nodes, output_nodes, output_function)
    elif network_type == NetworkType.OptimalBrainDamageNetwork:
        trainer = OptimalBrainDamageTrainer([(candidate_pool_size, activation_function)])
        net = PrunableCCNetwork(input_nodes, output_nodes, output_function)
    else:
        raise NotImplementedError("Unknown network type")

    net_id = unique_id()
    net = trainer.train_network(training_inputs, training_outputs, net)

    if save_network:
        with open(f"{output_dir}/{net_id}.pickle", 'wb') as f:
            dump(net, f)

    return net, net_id, trainer.epoch


def output_plot(inputs, results, net_id, model, dataset, data_type, output_dir):
    # Default values

    filename = f"{output_dir}/plot_{dataset}_{model}_{data_type}_{net_id}.png"

    pos_values = []
    neg_values = []

    # Separate positive and negative values for plotting
    for i in range(len(inputs)):
        if results[i][0] > 0:
            pos_values.append(inputs[i])
        elif results[i][0] <= 0:
            neg_values.append(inputs[i])

    # Extract x and y values for positive and negative cases
    pos_x, pos_y = zip(*pos_values)
    neg_x, neg_y = zip(*neg_values)

    fig = plt.figure()
    # Plot positive values in red and negative values in green
    plt.scatter(pos_x, pos_y, c='red', marker='o', label='pos')
    plt.scatter(neg_x, neg_y, c='green', marker='o', label='neg')

    # Set plot title and labels
    plt.title("Activation Pattern for the Output Unit")
    plt.xlabel("X-axis")
    plt.ylabel("Y-axis")

    # Display legend
    plt.legend()

    # Save or show the plot
    fig.savefig(filename)
    plt.close(fig)
    # plt.show()
    return filename


def run_with_testing(dataset: str, trial_configs: List[tuple], training_data: Dict[str, List],
                     validation_data: Dict[str, List], test_data: Dict[str, List],
                     num_inputs: int, num_outputs: int, activation_function: ActivationFunction,
                     output_function: ActivationFunction, asymmetric: bool, output_dir: str) -> None:

    with open("/".join([output_dir, "training.csv"]), 'w') as training_f:
        t_writer = csv.writer(training_f)
        t_writer.writerow(("dataset", "model", "id", "epochs",
                           "total_units", "hidden_units", "inner_connections",
                           "output_connections"))

        with open("/".join([output_dir, "results.csv"]), 'w') as f:
            writer = csv.writer(f)
            writer.writerow(("dataset", "model", "id", "data_type", "correct", "observations"))

            for model, network_type in trial_configs:
                # Train the network
                net, net_id, epoch = train(training_data['inputs'], training_data['outputs'],
                                           input_nodes=num_inputs, output_nodes=num_outputs,
                                           save_network=True,
                                           network_type=network_type, activation_function=activation_function,
                                           output_function=output_function, output_dir=output_dir)
                t_writer.writerow((dataset, model, net_id, epoch, net.number_of_nodes, net.number_of_hidden_nodes,
                                   net.number_inner_connections,
                                   net.number_of_output_connections))

                # run the training data through the network and check the results
                run_data_and_record_results(net=net, dataset=dataset, net_id=net_id, model=model,
                                            data=training_data, writer=writer, data_type="training",
                                            asymmetric=asymmetric, output_dir=output_dir)
                run_data_and_record_results(net=net, dataset=dataset, net_id=net_id, model=model,
                                            data=validation_data, writer=writer, data_type="validation",
                                            asymmetric=asymmetric, output_dir=output_dir)
                run_data_and_record_results(net=net, dataset=dataset, net_id=net_id, model=model,
                                            data=test_data, writer=writer, data_type="test",
                                            asymmetric=asymmetric, output_dir=output_dir)


def flatten_chain(matrix):
    return list(chain.from_iterable(matrix))


def run_data_and_record_results(net: CCNetwork, net_id: str, model: str,
                                dataset: str,
                                data: Dict[str, List], writer: csv.writer, data_type: str,
                                asymmetric: bool, output_dir):
    result = [(net.full_forward_pass(x), [node.value for node in net.output_nodes])[1] for x in data['inputs']]
    with (open("/".join([output_dir, f"raw_{dataset}_{model}_{data_type}_{net_id}.csv"]), 'w') as raw):
        raw_writer = csv.writer(raw)
        headers = ["dataset", "model", "data_type", "id"]
        headers.extend([f"input_{x}" for x in range(net.number_of_inputs)])
        headers.extend([f"actual_{x}" for x in range(net.number_of_outputs)])
        headers.extend([f"expected_{x}" for x in range(net.number_of_outputs)])
        raw_writer.writerow(headers)
        result_expected_pairs = tuple(zip(data['inputs'], result, data['outputs']))

        for result_expected in result_expected_pairs:
            row = [dataset, model, data_type, net_id]
            row.extend(flatten_chain(result_expected))
            raw_writer.writerow(row)

    correct = proportion_correct(data['outputs'], result, asymmetric)
    writer.writerow((dataset, model, net_id, data_type, correct, len(data['inputs'])))

    if net.number_of_outputs == 1 and net.number_of_inputs == 2 and not asymmetric:
        output_plot(data['inputs'], result, model=model, dataset=dataset, net_id=net_id,
                    data_type=data_type, output_dir=output_dir)

    return writer


def create_output_directory() -> str:
    path = f"result/{unique_id()}"
    Path(path).mkdir(parents=True, exist_ok=True)
    return path


def main(dataset: str, trials: int):
    trial_sets = []
    [trial_sets.extend(
        [("obdcc", NetworkType.OptimalBrainDamageNetwork),
         ("cc", NetworkType.StandardNetwork),
         ("pcc", NetworkType.PrunableNetwork)]) for _ in range(trials)]

    activation_function = Sigmoid()
    output_function = Sigmoid()
    asymmetric = False

    if dataset == "cancer":
        training_data, validation_data, test_data, metadata = load_problem("./proben1/cancer/cancer1.dt")
        num_inputs = metadata["bool_in"] or metadata["real_in"]
        num_outputs = metadata["bool_out"] or metadata["real_out"]
        output_function = AsymmetricSigmoid()
        asymmetric = True
    elif dataset == "glass":
        training_data, validation_data, test_data, metadata = load_problem("./proben1/glass/glass1.dt")
        num_inputs = metadata["bool_in"] or metadata["real_in"]
        num_outputs = metadata["bool_out"] or metadata["real_out"]
        output_function = AsymmetricSigmoid()
        asymmetric = True
    elif dataset == "spirals":
        training_data, validation_data, test_data = partitioned_spirals()
        num_inputs = 2
        num_outputs = 1
    elif dataset == "xor":
        xor_inputs = [
            [0, 0],
            [0, 1],
            [1, 0],
            [1, 1]
        ]
        xor_targets = [
            [-0.5],
            [0.5],
            [0.5],
            [-0.5]
        ]
        training_data = {"inputs": xor_inputs, "outputs": xor_targets}
        validation_data = {"inputs": xor_inputs, "outputs": xor_targets}
        test_data = {"inputs": xor_inputs, "outputs": xor_targets}
        num_inputs = 2
        num_outputs = 1
    else:
        raise NotImplementedError("Unknown Example")

    output_dir = create_output_directory()

    run_with_testing(dataset, trial_sets, training_data, validation_data,
                     test_data, num_inputs, num_outputs,
                     activation_function, output_function, asymmetric, output_dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("example", choices=['spirals', 'cancer', 'glass', 'xor'], default="spirals", type=str)
    parser.add_argument("trials", default=1, type=int)

    args = parser.parse_args()

    main(args.example, args.trials)
