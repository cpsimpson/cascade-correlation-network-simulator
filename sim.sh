#!/usr/bin/env bash

if test ! -d venv; then
  python3 -m venv venv
  . venv/bin/activate
  pip install .
else
  . venv/bin/activate
fi

# shellcheck disable=SC2034
for i in {1..100}; do python examples/simulation.py cancer 1; done