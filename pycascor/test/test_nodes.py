from unittest import TestCase
from pycascor.network import CCNetwork
from pycascor.activation_functions import Sigmoid


class TestNetwork(TestCase):
    def test_set_inputs(self):
        net = CCNetwork(2, 1, Sigmoid())

        net.set_inputs([10.2, 20.1])

        self.assertAlmostEqual(net.input_nodes[0].value, 1.0)
        self.assertAlmostEqual(net.input_nodes[1].value, 10.2)
        self.assertAlmostEqual(net.input_nodes[2].value, 20.1)
